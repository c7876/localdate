package org.example.localdate;

public class LocalDateApi implements DateApi {

    @Override
    public String getCurrentDateAsISOString() {
        return null;
    }

    @Override
    public boolean areDatesEqual(String firstDateAsString, String secondDateAsString) {
        return false;
    }

    @Override
    public boolean isFirstDateBeforeSecondDate(String firstDateAsString, String secondDateAsString) {
        return false;
    }

    @Override
    public boolean isFirstDateAfterSecondDate(String firstDateAsString, String secondDateAsString) {
        return false;
    }

    @Override
    public boolean isFirstDateBeforeOrEqualSecondDate(String firstDateAsString, String secondDateAsString) {
        return false;
    }
}
