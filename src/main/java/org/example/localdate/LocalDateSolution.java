package org.example.localdate;

import java.time.LocalDate;

public class LocalDateSolution implements DateApi {

    static class LocalDateUtilities {
        private LocalDate toLocalDate(String dateAsIsoString) {
            return LocalDate.parse(dateAsIsoString);
        }
    }

    private LocalDateUtilities localDateUtilities = new LocalDateUtilities();

    @Override
    public String getCurrentDateAsISOString() {
        return LocalDate.now().toString();
    }

    @Override
    public boolean areDatesEqual(String firstDateAsString, String secondDateAsString) {
        final LocalDate firstDate = localDateUtilities.toLocalDate(firstDateAsString);
        final LocalDate secondDate = localDateUtilities.toLocalDate(secondDateAsString);
        // return firstDate.equals(secondDate); // accept any object
        return firstDate.isEqual(secondDate); // preferred way as is type safe
        // return firstDate.compareTo(secondDate) == 0; // it is more verbose
    }

    @Override
    public boolean isFirstDateBeforeSecondDate(String firstDateAsString, String secondDateAsString) {
        final LocalDate firstDate = localDateUtilities.toLocalDate(firstDateAsString);
        final LocalDate secondDate = localDateUtilities.toLocalDate(secondDateAsString);

        return firstDate.isBefore(secondDate);
    }

    @Override
    public boolean isFirstDateAfterSecondDate(String firstDateAsString, String secondDateAsString) {
        final LocalDate firstDate = localDateUtilities.toLocalDate(firstDateAsString);
        final LocalDate secondDate = localDateUtilities.toLocalDate(secondDateAsString);
        return firstDate.isAfter(secondDate);
    }

    @Override
    public boolean isFirstDateBeforeOrEqualSecondDate(String firstDateAsString, String secondDateAsString) {
        final LocalDate firstDate = localDateUtilities.toLocalDate(firstDateAsString);
        final LocalDate secondDate = localDateUtilities.toLocalDate(secondDateAsString);
        // return firstDate.compareTo(secondDate) <= 0;
        return firstDate.isBefore(secondDate) || firstDate.isEqual(secondDate); // preferred way more readable
    }


}
