package org.example.localdate;

/**
 * Use string in ISO format for dates to be as generic as possible not dependent on the java api regarding date/time which will implement the methods
 */
public interface DateApi {

    String getCurrentDateAsISOString();

    boolean areDatesEqual(String firstDateAsString, String secondDateAsString);

    boolean isFirstDateBeforeSecondDate(String firstDateAsString, String secondDateAsString);

    boolean isFirstDateAfterSecondDate(String firstDateAsString, String secondDateAsString);

    boolean isFirstDateBeforeOrEqualSecondDate(String firstDateAsString, String secondDateAsString);
}
