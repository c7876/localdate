package org.example.localdate;

public interface DateTimeApi {
    String getCurrentDateTimeAsString();
}
