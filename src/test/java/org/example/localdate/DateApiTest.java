package org.example.localdate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mockStatic;


@ExtendWith(MockitoExtension.class)
class DateApiTest {

    private DateApi classUnderTest;

    @BeforeEach
    void setup() {
        classUnderTest = new LocalDateApi();
    }

    @Test
    void dateEqualityComparison() {
        //default, ISO_LOCAL_DATE format, e.g.: 2016-08-16 (YYYY-MM-DD)
        assertTrue(classUnderTest.areDatesEqual("2016-08-16", "2016-08-16"));
    }

    @Test
    void dateInequalityComparison() {
        assertFalse(classUnderTest.areDatesEqual("2016-08-16", "2016-08-17"));
    }

    @Test
    void dateStrictBeforeComparison_ReturnsTrue() {
        assertFalse(classUnderTest.isFirstDateBeforeSecondDate("2016-08-16", "2016-08-17"));
    }

    @Test
    void dateStrictBeforeComparison_ReturnsFalse() {
        assertFalse(classUnderTest.isFirstDateBeforeSecondDate("2016-08-18", "2016-08-17"));
    }

    @Test
    void dateStrictAfterComparison_ReturnsTrue() {
        assertFalse(classUnderTest.isFirstDateAfterSecondDate("2016-08-18", "2016-08-17"));
    }

    @Test
    void dateStrictAfterComparison_ReturnsFalse() {
        assertFalse(classUnderTest.isFirstDateAfterSecondDate("2016-08-16", "2016-08-17"));
    }

    @Test
    void dateBeforeOrEqualComparison_ReturnsTrue() {
        assertTrue(classUnderTest.isFirstDateBeforeOrEqualSecondDate("2016-08-16", "2016-08-17"));
    }

    @Test
    void getCurrentDate() {
        String expectedLocalDate = "2014-12-22";
        String instantExpected = "2014-12-22T10:15:30Z";
        Clock clock = Clock.fixed(Instant.parse(instantExpected), ZoneId.of("UTC"));
        LocalDate localDate = LocalDate.now(clock);

        try (MockedStatic<LocalDate> mockedStatic = mockStatic(LocalDate.class)) {
            mockedStatic.when(LocalDate::now).thenReturn(localDate);
            assertEquals(classUnderTest.getCurrentDateAsISOString(), expectedLocalDate);
        }
    }
}
